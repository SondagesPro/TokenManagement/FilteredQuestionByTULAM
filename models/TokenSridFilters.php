<?php

/**
 * This file is part of FilteredQuestionByTULAM plugin
 * @version 0.0.0
 */

namespace FilteredQuestionByTULAM\models;

use Yii;
use CActiveRecord;
use CDbCriteria;

class TokenSridFilters extends CActiveRecord
{
    /**
     * Class responseListAndManage\models\TokenSridAccess
     *
     * @property integer $sid survey
     * @property string $token : token
     * @property string $filter : json encoded filter
    */

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /** @inheritdoc */
    public function tableName()
    {
        return '{{filteredquestionbytulam_tokensrid}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return array('sid', 'token');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('sid', 'required'),
            array('token', 'required'),
            array('sid', 'numerical', 'integerOnly' => true),
            array('token', 'unique', 'criteria' => array(
                    'condition' => 'sid=:sid',
                    'params' => array(':sid' => $this->sid )
                ),
                'message' => sprintf("Token : '%s' already set for '%s'.", $this->token, $this->sid),
            ),
        );
        return $aRules;
    }
}

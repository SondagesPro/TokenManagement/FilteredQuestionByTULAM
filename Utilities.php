<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 201 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.0.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace FilteredQuestionByTULAM;

class Utilities
{
    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'useAttributeFiltered' => 0,
    );

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSetting($surveyId, $sSetting)
    {
         $oPlugin = \Plugin::model()->find(
             "name = :name",
             array(":name" => 'FilteredQuestionByTULAM')
         );
        /* How can i come here ? */
        if (!$oPlugin) {
            return null;
        }
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if ($value !== '') {
                return $value;
            }
        }
        if (in_array($sSetting, self::GlobalSettings)) {
            $oSetting = \PluginSetting::model()->find(
                'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model IS NULL',
                array(
                    ':pluginid' => $oPlugin->id,
                    ':key' => $sSetting,
                )
            );
            if (!empty($oSetting)) {
                $value = json_decode($oSetting->value);
                if ($value !== '') {
                    return $value;
                }
            }
        }

        /* empty (string) : then get setting by default */
        if (isset(self::DefaultSettings[$sSetting])) {
            return self::DefaultSettings[$sSetting];
        }
        return null;
    }
}

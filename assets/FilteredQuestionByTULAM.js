/**
 * Action for FilteredQuestionByTULAM
 * @author Denis Chenu
 * @licence MIT
 * @version 0.0.1
 */
var FilteredQuestionByTULAM = {
    init: function () {
        $(".tulam-filter-group-item").each(function () {
            if ($(this).find(":checkbox").length == $(this).find(":checkbox:checked").length) {
                $(this).closest("fieldset").find(".tulam-filter-group-checkbox").prop('checked', true);
            }
        });
        $(".tulam-filter-legend").on('click', function (event) {
            $(this).find("input:checkbox").trigger("click");
        });
        $(".tulam-filter-group-checkbox").on('click', function (event) {
            event.stopPropagation();
        });
        $(".tulam-filter-group-checkbox").on('change', function (event) {
            var related = $(this).attr('id');
            $("input:checkbox[data-related-group='" + related + "']").prop(
                "checked",
                $(this).prop('checked')
            );
        });
        $("input:checkbox[data-related-group]").on('change', function (event) {
            if ($(this).closest(".tulam-filter-group-item").find(":checkbox").length == $(this).closest(".tulam-filter-group-item").find(":checkbox:checked").length) {
                $("#" + $(this).data("related-group")).prop('checked', true);
            } else {
                $("#" + $(this).data("related-group")).prop('checked', false);
            }
        });
        $('.tulam-filter-label, .tulam-filter-legend').tooltip()
    }
};

<?php

/**
 * Filtered question using TULAM manager
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2025 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 * @version 0.5.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class FilteredQuestionByTULAM extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Filtered question system using TULAM manager';
    protected static $name = 'FilteredQuestionByTULAM';

    /* @var integer $DBVERSION currently setup */
    const DBVERSION = 1;

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'useAttributeFiltered' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Use filtered attribute by default.",
            'default' => 0,
        ),
    );

    /** @var string[] question to be hidden */
    private $hiddenQuestions = null;
    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        Yii::setPathOfAlias('FilteredQuestionByTULAM', dirname(__FILE__));

        $this->subscribe('beforeActivate');

        /* The survey setting */
        $this->subscribe('beforeTULAMSurveySettings');
        $this->subscribe('newTULAMSurveySettings');

        /* Tulam system update */
        $this->subscribe('getTULAMUserFormData');
        $this->subscribe('beforeTULAMSaveSetUserAttribute', 'SaveSetUserAttribute');
        $this->subscribe('TULAMRegisterPackage', 'createAndRegisterPackageFilteredQuestionByTULAM');

        /* Disable update */
        $this->subscribe('beforeSurveyPage');
    }

    /**
    * Check if can be used
    * @see parent::init()
    */
    public function beforeActivate()
    {
        if (!defined('\TokenUsersListAndManagePlugin\Utilities::API') || \TokenUsersListAndManagePlugin\Utilities::API < 0.12) {
            $this->getEvent()->set('success', false);
            $this->getEvent()->set(
                'message',
                sprintf(
                    $this->translate("You need %s plugin version %s and up"),
                    CHTml::link(
                        'TokenUsersListAndManagePlugin',
                        "https://extensions.sondages.pro/TokenUsersListAndManage.html",
                        array('target' => '_blank')
                    ),
                    "0.12"
                )
            );
            return;
        }
        $this->setDB();
    }

    /**
    * Add the settings
    */
    public function beforeTULAMSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $useAttributeFilteredDefault = $this->get('useAttributeFiltered', null, null, 0) ? gT('Yes') : gT('No');
        $aTokensAttributeList = $SurveySettingsEvent->get('aTokensAttributeList');
        if (empty($aTokensAttributeList)) {
            $aTokensAttributeList = array();
        }
        $SurveySettingsEvent->set("settings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'tokenAttributeFiltered' => array(
                    'type' => 'select',
                    'label' => $this->translate('Token attributes to set filter on question'),
                    'help' => $this->translate('Any value except 0 and empty string set this to true.'),
                    'options' => $aTokensAttributeList,
                    'htmlOptions' => array(
                        'empty' => $this->translate("None"),
                    ),
                    'current' => $this->get('tokenAttributeFiltered', 'Survey', $surveyId)
                ),
                'useAttributeFiltered' => array(
                    'type' => 'select',
                    'label' => $this->translate('Use filtered attribute.'),
                    'help' => $this->translate('Use filtered attribute for this survey.'),
                    'options' => array(
                        '0' => gT("No"),
                        '1' => gT("Yes"),
                    ),
                    'htmlOptions' => array(
                        'empty' => sprintf($this->translate("Leave default (%s)"), $useAttributeFilteredDefault)
                    ),
                    'current' => $this->get('useAttributeFiltered', 'Survey', $surveyId, '')
                ),
                //~ 'tokenAttributeFilter' => array(
                    //~ 'type' => 'select',
                    //~ 'label' => $this->translate('Token attributes for the question filters'),
                    //~ 'help' => $this->translate('If not set usage of included table.'),
                    //~ 'options' => $aTokensAttributeList,
                    //~ 'htmlOptions' => array(
                        //~ 'empty' => $this->translate("None"),
                    //~ ),
                    //~ 'current' => $this->get('tokenAttributeFilter', 'Survey', $surveyId)
                //~ ),
            ),
        ));
    }

    /**
    * Save the settings
    */
    public function newTULAMSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $settings = $this->getEvent()->get('settings');
        $usedAttributes = $this->getEvent()->get('usedAttributes');
        $tokenAttributeFiltered = "";
        if (!empty($settings['tokenAttributeFiltered'])) {
            if (in_array($settings['tokenAttributeFiltered'], $usedAttributes)) {
                $settings['tokenAttributeFiltered'] = "";
            } else {
                $usedAttributes['FilteredQuestionByTULAM_tokenAttributeFiltered'] = $settings['tokenAttributeFiltered'];
                $this->getEvent()->set('usedAttributes', $usedAttributes);
            }
        }
        if (isset($settings['tokenAttributeFiltered'])) {
            $this->set('tokenAttributeFiltered', $settings['tokenAttributeFiltered'], 'Survey', $surveyId);
        }
        if (isset($settings['useAttributeFiltered'])) {
            $this->set('useAttributeFiltered', $settings['useAttributeFiltered'], 'Survey', $surveyId);
        }
    }

    /**
    * Add the edit attribute system
    */
    public function getTULAMUserFormData()
    {
        $userFormDataEvent = $this->getEvent();
        $surveyId = $userFormDataEvent->get('surveyid');
        $originalSurveyId = $userFormDataEvent->get('originalSurveyId');
        $userToken = $userFormDataEvent->get('usertoken');
        $userFormData = $userFormDataEvent->get('userFormData');
        $tokenAttributeFiltered = $this->get('tokenAttributeFiltered', 'Survey', $surveyId);

        $aFilteredSurveys = $this->getFilteredSurveyId($surveyId, $originalSurveyId);
        if (empty($aFilteredSurveys)) {
            return;
        }
        $allAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        $aTokenAttributeFiltered = $allAttributes[$tokenAttributeFiltered];
        $aTokenAttributeFiltered['type'] = 'filtered';
        $aTokenAttributeFiltered['mandatory'] = 'N';
        $aTokenAttributeFiltered['notin'] = array('myaccount');
        $aTokenAttributeFiltered['useAttributeFiltered'] = $this->getSetting($originalSurveyId, 'useAttributeFiltered');
        $aTokenAttributeFiltered['title'] = $this->gT("Manage filter");
        if (count($aFilteredSurveys) > 1) {
            $aTokenAttributeFiltered['title'] = $this->gT("Manage filters");
        }
        if (empty($aTokenAttributeFiltered['description'])) {
            $aTokenAttributeFiltered['description'] = $aTokenAttributeFiltered['caption'];
            if (empty($aTokenAttributeFiltered['description']) || $aTokenAttributeFiltered['description'] == $tokenAttributeFiltered) {
                $aTokenAttributeFiltered['description'] = self::translate("Is filtered");
            }
        }
        $aTokenAttributeFiltered['help'] = $this->translate("Check all questions to be hidden from user");
        $aTokenAttributeFiltered['filtered'] = array();
        foreach ($aFilteredSurveys as $filterSurveyId) {
            $oSurvey = Survey::model()->active()->with('languagesettings')->findByPk($filterSurveyId);
            $aTokenAttributeFiltered['filtered'][$filterSurveyId] = array(
                'title' => $oSurvey->getLocalizedTitle(),
                'aGroupsQuestions' => $this->getGroupsQuestions($filterSurveyId),
                'aHiddens' => $this->getHiddenList($filterSurveyId, $userToken),
            );
        }
        $userFormData['attributes'][$tokenAttributeFiltered] = $aTokenAttributeFiltered;
        $userFormDataEvent->set('userFormData', $userFormData);
        $this->subscribe('getPluginTwigPath');
    }

    public function SaveSetUserAttribute()
    {
        $beforeSaveUserEvent = $this->getEvent();
        $surveyId = $beforeSaveUserEvent->get('surveyid');
        $originalSurveyId = $beforeSaveUserEvent->get('originalSurveyId');
        $aFilteredSurveys = $this->getFilteredSurveyId($surveyId, $originalSurveyId);
        if (empty($aFilteredSurveys)) {
            return;
        }
        $userToken = $beforeSaveUserEvent->get('usertoken');
        $oUserToken = $beforeSaveUserEvent->get('oUserToken');
        if (empty($userToken)) {
            $userToken = $oUserToken->token;
        }
        if (empty($userToken)) {
            /* To do log it */
            return;
        }

        /* Save current attribute (if not in register part) */
        $newPostAttributes = $beforeSaveUserEvent->get('newPostAttributes');
        $tokenAttributeFiltered = $this->get('tokenAttributeFiltered', 'Survey', $surveyId);
        $postAttributeFiltered = App()->getRequest()->getPost($tokenAttributeFiltered);
        $attributeFiltered = isset($newPostAttributes[$tokenAttributeFiltered]) ? $newPostAttributes[$tokenAttributeFiltered] : "";
        $oUserToken->$tokenAttributeFiltered = $attributeFiltered;
        $beforeSaveUserEvent->set('oUserToken', $oUserToken);

        /* Save the filter */
        Yii::setPathOfAlias('FilteredQuestionByTULAM', dirname(__FILE__));
        foreach ($aFilteredSurveys as $filteredSurveyid) {
            if (empty($postAttributeFiltered['filters'][$filteredSurveyid])) {
                \FilteredQuestionByTULAM\models\TokenSridFilters::model()->deleteByPk(array(
                    'sid' => $filteredSurveyid,
                    'token' => $userToken
                ));
                continue;
            }
            $aQuestionsTitle = (array) $postAttributeFiltered['filters'][$filteredSurveyid];
            $aQuestionsTitle = array_values(array_filter($aQuestionsTitle));
            $aAllQuestion = $this->getGroupsQuestions($filteredSurveyid, null, false);
            $aExistingCode = array_map(function ($question) {
                return $question['code'];
            }, $aAllQuestion);
            $aQuestionsTitle = array_intersect($aQuestionsTitle, $aExistingCode);

            if (empty($aQuestionsTitle)) {
                \FilteredQuestionByTULAM\models\TokenSridFilters::model()->deleteByPk(array(
                    'sid' => $filteredSurveyid,
                    'token' => $userToken
                ));
                continue;
            }
            $oTokenSridFilter = \FilteredQuestionByTULAM\models\TokenSridFilters::model()->findByPk(array(
                'sid' => $filteredSurveyid,
                'token' => $userToken
            ));
            if (empty($oTokenSridFilter)) {
                $oTokenSridFilter = new \FilteredQuestionByTULAM\models\TokenSridFilters();
                $oTokenSridFilter->sid = $filteredSurveyid;
                $oTokenSridFilter->token = $userToken;
            }
            $oTokenSridFilter->filter = json_encode($aQuestionsTitle);
            $oTokenSridFilter->save();
        }
    }

    /**
     * Get the list of survey id according to the original and current
     * @param integer $surveyId
     * @param integer $originalSurveyId
     * @return null|integer(]
     */
    private function getFilteredSurveyId($surveyId, $initialSurveyId)
    {
        $tokenAttributeFiltered = $this->get('tokenAttributeFiltered', 'Survey', $surveyId);
        $allAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        if (empty($allAttributes[$tokenAttributeFiltered])) {
            return null;
        }
        if ($initialSurveyId != $surveyId) {
            if ($this->get('tokenAttributeFiltered', 'Survey', $initialSurveyId) != $tokenAttributeFiltered) {
                /* Todo : log it */
                /* Todo : get it by parent (always, according to TokenUserManegmùent setting) */
                return null;
            }
            $allOriginalAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
            if (empty($allOriginalAttributes[$tokenAttributeFiltered])) {
                /* Todo : log it */
                return null;
            }
        }
        if ($this->getSetting($initialSurveyId, 'useAttributeFiltered')) {
            return array($initialSurveyId);
        }

        $oChildSurveys = new \RelatedSurveyManagement\ChildrenSurveys($surveyId);
        $aChildrensSurveys = $oChildSurveys->getChildrensSurveys();
        $aFilteredSurveys = array();
        foreach ($aChildrensSurveys as $childSurveyId) {
            if ($this->getSetting($childSurveyId, 'useAttributeFiltered')) {
                if ($this->get('tokenAttributeFiltered', 'Survey', $childSurveyId) == $tokenAttributeFiltered) {
                    $aFilteredSurveys[] = $childSurveyId;
                } else {
                    /* Todo : log it */
                }
            }
        }
        if (empty($aFilteredSurveys)) {
            return null;
        }
        return $aFilteredSurveys;
    }

    /**
     * Add some views for this and other plugin
     */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
    * Hide the question
    */
    public function beforeQuestionRender()
    {
        if (!in_array($this->getEvent()->get('code'), $this->hiddenQuestions)) {
            return;
        }
        $oEvent = $this->getEvent();
        $oEvent->set('text', '');
        $oEvent->set('questionhelp', '');
        $oEvent->set('class', $oEvent->get('class') . ' hidden disable-edit tulam-question-hidden');
        $aHtmlOptions = $oEvent->get('aHtmlOptions');
        $aHtmlOptions['style'] = "display:none";
        $oEvent->set('aHtmlOptions', $aHtmlOptions);
    }

    /**
    * Disable update and register to beforeQuestionrender if needed
    */
    public function beforeSurveyPage()
    {
        $surveyId = $this->getEvent()->get('surveyId');
        if (!$this->getSetting($surveyId, 'useAttributeFiltered')) {
            return;
        }
        if (!$this->getSetting($surveyId, 'tokenAttributeFiltered')) {
            return;
        }
        $tokenAttributeFiltered = $this->getSetting($surveyId, 'tokenAttributeFiltered');
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if (empty($token)) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey->getHasTokensTable()) {
            return;
        }
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken) || empty($oToken->$tokenAttributeFiltered) || trim($oToken->$tokenAttributeFiltered) == "" || trim($oToken->$tokenAttributeFiltered) == "0") {
            return;
        }
        $this->hiddenQuestions = $this->getHiddenList($surveyId, $token);
        if (empty($this->hiddenQuestions)) {
            return;
        }
        /* Hide question */
        $this->subscribe('beforeQuestionRender');
    }

    /**
    * @see parent::gT
    */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return $this->gT($sToTranslate, $sEscapeMode, $sLanguage);
    }

    /**
     * Create the needed DB
     */
    private function setDb()
    {
        if (intval($this->get("dbVersion")) >= self::DBVERSION) {
            return;
        }
        if (!$this->api->tableExists($this, 'tokensrid')) {
            $this->api->createTable($this, 'tokensrid', array(
                'sid' => "int NOT NULL",
                'token' => "string(100) NOT NULL DEFAULT ''",
                'filter' => "longtext",
            ));
            $tableName = $this->api->getTable($this, 'tokensrid')->tableName();
            App()->getDb()->createCommand()->addPrimaryKey('tokensrid_sidtoken', $tableName, 'sid,token');
            $this->set("dbVersion", self::DBVERSION);
            return;
        }
    }

    /** Get the questions as array with group for construct the panel
     * @param integer $surveyId
     * @param string $language
     * @param boolean $bygroup
     * @return array fpor twig generation
     */
    private function getGroupsQuestions($surveyId, $language = null, $bygroup = true)
    {
        if (intval(App()->getConfig('versionnumber')) <= 3) {
            return $this->getGroupsQuestionsLegacy3($surveyId, $language, $bygroup);
        }
        if (empty($language)) {
            $language = App()->getLanguage();
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }

        Yii::import('application.helpers.viewHelper');
        /* Needed */
        //$this->subscribe("getPluginTwigPath");
        $questionTable = Question::model()->tableName();
        $command = Yii::app()->db->createCommand()
            ->select("qid,{{questions}}.gid as gid,{{groups}}.group_order, {{questions}}.question_order")
            ->from($questionTable)
            ->where("({{questions}}.sid = :sid AND {{questions}}.parent_qid = 0)")
            ->join("{{groups}}", "{{groups}}.gid = {{questions}}.gid")
            ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
            ->bindParam(":sid", $surveyId, PDO::PARAM_INT);
        $allQuestions = $command->query()->readAll();
        if ($bygroup) {
            $aGroups = array();
        } else {
            $aQuestions = array();
        }
        $gid = 0;
        foreach ($allQuestions as $question) {
            $qid = $question['qid'];
            if ($bygroup && $gid != $question['gid']) {
                if ($gid) {
                    if (empty($aQuestions)) {
                        unset($aGroups[$gid]);
                    } else {
                        $aGroups[$gid]['questions'] = $aQuestions;
                    }
                }
                $gid = $question['gid'];
                $text = $gid;
                $oGroupL10n = QuestionGroupL10n::model()->find("gid = :gid and language = :language", array(":gid" => $gid,":language" => $language));
                if ($oGroupL10n) {
                    $text = viewHelper::purified($oGroupL10n->group_name);
                }
                $aGroups[$gid] = array(
                    'gid' => $gid,
                    'title' => $text,
                    'questions' => array(),
                );
                $aQuestions = array();
            }
            $gid = $question['gid'];
            $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
            $oQuestionL10n = QuestionL10n::model()->find("qid = :qid and language = :language", array(":qid" => $qid,":language" => $language));
            $oHiddenAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = 'hidden'",
                array( ":qid" => $qid)
            );
            if (!empty($oHiddenAttribute) && !empty($oHiddenAttribute->value)) {
                /* hidden by attribute */
                continue;
            }
            $oCssclassAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = 'cssclass'",
                array( ":qid" => $qid)
            );
            if ($oCssclassAttribute) {
                $cssclass = "# " . strval($oCssclassAttribute->value) . " ";
                if (strpos($cssclass, " hidden") || strpos($cssclass, " tulam-filter-hidden")) {
                    /* hidden by class or for tulam-filter */
                    continue;
                }
            }
            $text = $oQuestion->title;
            if ($oQuestionL10n) {
                $text = viewHelper::purified($oQuestionL10n->question);
            }
            $aQuestions[] = array(
                'qid' => $qid,
                'code' => $oQuestion->title,
                'question' => $text,
            );
        }
        /* Last group not closed */
        if ($bygroup && !empty($aQuestions)) {
            $aGroups[$gid]['questions'] = $aQuestions;
        }
        if ($bygroup) {
            return $aGroups;
        } else {
            return $aQuestions;
        }
    }

    /** Get the questions as array with group for construct the panel
     * @param integer $surveyId
     * @param string $language
     * @param boolean $bygroup
     * @return array fpor twig generation
     */
    private function getGroupsQuestionsLegacy3($surveyId, $language = null, $bygroup = true)
    {
        if (empty($language)) {
            $language = App()->getLanguage();
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }

        Yii::import('application.helpers.viewHelper');
        /* Needed */
        //$this->subscribe("getPluginTwigPath");
        $questionTable = Question::model()->tableName();
        $command = Yii::app()->db->createCommand()
            ->select("qid,{{questions}}.gid as gid,{{questions}}.language as language,{{groups}}.group_order, {{questions}}.question_order")
            ->from($questionTable)
            ->where("({{questions}}.sid = :sid AND {{questions}}.language = :language AND {{questions}}.parent_qid = 0)")
            ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid  AND {{questions}}.language = {{groups}}.language")
            ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
            ->bindParam(":sid", $surveyId, PDO::PARAM_INT)
            ->bindParam(":language", $language, PDO::PARAM_STR);
        $allQuestions = $command->query()->readAll();
        if ($bygroup) {
            $aGroups = array();
        } else {
            $aQuestions = array();
        }
        $gid = 0;
        foreach ($allQuestions as $question) {
            $qid = $question['qid'];
            if ($bygroup && $gid != $question['gid']) {
                if ($gid) {
                    if (empty($aQuestions)) {
                        unset($aGroups[$gid]);
                    } else {
                        $aGroups[$gid]['questions'] = $aQuestions;
                    }
                }
                $gid = $question['gid'];
                $oGroup = QuestionGroup::model()->find("gid = :gid and language = :language", array(":gid" => $gid,":language" => $language));
                $aGroups[$gid] = array(
                    'gid' => $gid,
                    'title' => viewHelper::purified($oGroup->group_name),
                    'questions' => array(),
                );
                $aQuestions = array();
            }
            $gid = $question['gid'];
            $oQuestion = Question::model()->find("qid = :qid and language = :language", array(":qid" => $qid,":language" => $language));
            $oHiddenAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = 'hidden'",
                array( ":qid" => $qid)
            );
            if (!empty($oHiddenAttribute) && !empty($oHiddenAttribute->value)) {
                /* hidden by attribute */
                continue;
            }
            $oCssclassAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = 'cssclass'",
                array( ":qid" => $qid)
            );
            if ($oCssclassAttribute) {
                $cssclass = "# " . strval($oCssclassAttribute->value) . " ";
                if (strpos($cssclass, " hidden") || strpos($cssclass, " tulam-filter-hidden")) {
                    /* hidden by class or for tulam-filter */
                    continue;
                }
            }
            $aQuestions[] = array(
                'qid' => $qid,
                'code' => $oQuestion->title,
                'question' => viewHelper::purified($oQuestion->question),
                //'questions' => array(), To manage question inside question …
            );
        }
        /* Last group not closed */
        if ($bygroup && !empty($aQuestions)) {
            $aGroups[$gid]['questions'] = $aQuestions;
        }
        if ($bygroup) {
            return $aGroups;
        } else {
            return $aQuestions;
        }
    }

    /**
     * Get the visible list for surey with token
     * @param integer $filterSurveyId
     * @param sring $token
     * @return string[]
     */
    private function getHiddenList($filterSurveyId, $userToken)
    {
        if (empty($userToken)) {
            return array();
        }
        $oTokenSridFilter = \FilteredQuestionByTULAM\models\TokenSridFilters::model()->findByPk(array(
            'sid' => $filterSurveyId,
            'token' => $userToken
        ));
        if (empty($oTokenSridFilter)) {
            return array();
        }
        $filters = @json_decode($oTokenSridFilter->filter);
        if (empty($filters)) {
            return array();
        }
        return $filters;
    }

    /**
     * get a setting for a survey
     * @param integer $surveyId
     * @param string $setting
     * @return string
     */
    private function getSetting($surveyId, $setting)
    {
        $value = $this->get($setting, 'Survey', $surveyId, '');
        if ($value !== "") {
            return $value;
        }
        $value = $this->get($setting, null, null, '');
        if ($value !== "") {
            return $value;
        }
        if (isset($this->settings[$setting]['default'])) {
            return $this->settings[$setting]['default'];
        }
        return null;
    }

    /**
     * Check the javascript package
     * @return boolean
     */
    public function createAndRegisterPackageFilteredQuestionByTULAM()
    {
        if (!App()->clientScript->hasPackage('FilteredQuestionByTULAM')) {
            App()->clientScript->addPackage('FilteredQuestionByTULAM', array(
                'basePath'    => 'FilteredQuestionByTULAM.assets',
                'css'         => array(
                    'FilteredQuestionByTULAM.css'
                ),
                'js'          => array(
                    'FilteredQuestionByTULAM.js'
                ),
                'depends' => array(
                    'jquery',
                    'bootstrap',
                )
            ));
        }
        App()->getClientScript()->registerPackage('FilteredQuestionByTULAM');
    }
}
